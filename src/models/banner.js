'use strict';
module.exports = (sequelize, DataTypes) => {
  const banner = sequelize.define('Banner', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    imageUrl: DataTypes.TEXT,
    destinationUrl: DataTypes.TEXT,
    isActive: DataTypes.BOOLEAN
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "banner",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  banner.associate = function(models) {
    // associations can be defined here
  };
  return banner;
};