'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('Post', {
    title: DataTypes.STRING,
    category: DataTypes.STRING,
    slug: DataTypes.STRING,
    body: DataTypes.TEXT,
    featuredImageUrl: DataTypes.TEXT,
    status: DataTypes.STRING,
    publishedDate: DataTypes.DATE,
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "post",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  post.associate = function(models) {
    // associations can be defined here
  };

  post.beforeCreate((post, options) => {
    const title = post.title.toLowerCase()
    const slug = title.replace(' ', '-')

    post.slug = `${slug}-${new Date().getTime()}` 
  })
  return post;
};