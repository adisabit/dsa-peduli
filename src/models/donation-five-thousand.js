'use strict';
module.exports = (sequelize, DataTypes) => {
  const DonationFiveThousand = sequelize.define('DonationFiveThousand', {
    donationId: DataTypes.INTEGER,
    age: DataTypes.STRING,
    domicile: DataTypes.STRING
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "donation_five_thousand",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  DonationFiveThousand.associate = function(models) {
    // associations can be defined here
  };
  return DonationFiveThousand;
};