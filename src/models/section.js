'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const Section = sequelize.define('Section', {
    category: DataTypes.STRING,
    code: DataTypes.STRING,
    name: DataTypes.STRING,
    value: DataTypes.TEXT,
    isActive: DataTypes.BOOLEAN,
    createdAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.createdAt).format()
      }
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.updatedAt).format()
      }
    },
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "section",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  Section.associate = function (models) {
    // associations can be defined here
  };
  return Section;
};