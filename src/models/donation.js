'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const Donation = sequelize.define('Donation', {
    paymentMethodId: DataTypes.INTEGER,
    paymentMethod: DataTypes.STRING,
    campaignId: DataTypes.INTEGER,
    category: DataTypes.STRING,
    fullName: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    isAnonymous: DataTypes.BOOLEAN,
    donationTime: DataTypes.DATE,
    status: DataTypes.STRING
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "donation",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });

  Donation.beforeCreate((donation, options) => {
    donation.donationTime = moment().format()
  })

  Donation.associate = function(models) {
    // associations can be defined here
  };
  
  return Donation;
};