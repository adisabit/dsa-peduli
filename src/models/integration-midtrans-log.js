'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const IntegrationMidtransLog = sequelize.define('IntegrationMidtransLog', {
    url: DataTypes.STRING,
    requestUrl: DataTypes.STRING,
    requestMethod: DataTypes.STRING,
    requestHeader: DataTypes.TEXT,
    requestBody: DataTypes.TEXT,
    requestTime: DataTypes.DATE,
    serverMemoryUsage: DataTypes.FLOAT,
    responseCode: DataTypes.STRING,
    responseBody: DataTypes.TEXT,
    responseTime: DataTypes.DATE,
    createdAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.createdAt).format()
      }
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.updatedAt).format()
      }
    },
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "integration_midtrans_log",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  IntegrationMidtransLog.associate = function(models) {
    // associations can be defined here
  };
  return IntegrationMidtransLog;
};