'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const IntegrationMidtransCharge = sequelize.define('IntegrationMidtransCharge', {
    transactionId: DataTypes.STRING,
    orderId: DataTypes.STRING,
    merchantId: DataTypes.STRING,
    grossAmount: DataTypes.DECIMAL,
    currency: DataTypes.STRING,
    paymentType: DataTypes.STRING,
    transactionTime: DataTypes.DATE,
    transactionStatus: DataTypes.STRING,
    fraudStatus: DataTypes.STRING,
    payments: DataTypes.TEXT,
    createdAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.createdAt).format()
      }
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.updatedAt).format()
      }
    },
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "integration_midtrans_charge",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  IntegrationMidtransCharge.associate = function(models) {
    // associations can be defined here
  };
  return IntegrationMidtransCharge;
};