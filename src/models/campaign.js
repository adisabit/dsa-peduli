'use strict';
module.exports = (sequelize, DataTypes) => {
  const campaign = sequelize.define('Campaign', {
    category: DataTypes.STRING,
    featuredImageUrl: DataTypes.STRING,
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    targetAmount: DataTypes.FLOAT,
    targetDate: DataTypes.DATE
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "campaign",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  campaign.associate = function(models) {
    // associations can be defined here
    campaign.hasMany(models.Donation, {foreignKey: 'campaignId', as: 'donations'})
  };
  return campaign;
};