'use strict';
module.exports = (sequelize, DataTypes) => {
  const MasterPaymentMethod = sequelize.define('MasterPaymentMethod', {
    type: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    imageUrl: DataTypes.TEXT,
    code: DataTypes.STRING,
    paymentType: DataTypes.STRING,
    accountNumber: DataTypes.STRING,
    accountName: DataTypes.STRING,
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "master_payment_method",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  MasterPaymentMethod.associate = function(models) {
    // associations can be defined here
  };
  return MasterPaymentMethod;
};