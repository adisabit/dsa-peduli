'use strict';
module.exports = (sequelize, DataTypes) => {
  const DonationFidyah = sequelize.define('DonationFidyah', {
    donationId: DataTypes.INTEGER,
    address: DataTypes.TEXT,
    days: DataTypes.INTEGER
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "donation_fidyah",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  DonationFidyah.associate = function(models) {
    // associations can be defined here
  };
  return DonationFidyah;
};