'use strict';

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const subscription = sequelize.define('Subscription', {
    email: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.createdAt).format()
      }
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: () => {
        return moment(this.updatedAt).format()
      }
    },
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: "subscription",
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }
  });
  subscription.associate = function(models) {
    // associations can be defined here
  };
  return subscription;
};