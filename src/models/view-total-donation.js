'use strict';
module.exports = (sequelize, DataTypes) => {
  const ViewTotalDonation = sequelize.define('ViewTotalDonation', {
    totalDonation: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: "view_donation_total"
  });
  ViewTotalDonation.associate = function(models) {
    // associations can be defined here
  };
  return ViewTotalDonation;
};