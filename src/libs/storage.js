const admin = require('firebase-admin')
const { v4: uuidv4 } = require('uuid')
const fs = require('fs');
const appRoot = require('app-root-path');

// load .env if exist
if (fs.existsSync(`${appRoot}/.env`)) {
    require('dotenv').config()
}

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    storageBucket: process.env.BUCKET_NAME
});

var storage = admin.storage();

const upload = async (path, options) => {
    try {
        const uuid = uuidv4()

        await storage.bucket().upload(path, options);

        return `https://firebasestorage.googleapis.com/v0/b/${process.env.BUCKET_NAME}/o/${encodeURIComponent(options.destination)}?alt=media&token=${uuid}`

    } catch (error) {
        throw error 
    }
}

module.exports = {
    upload
}