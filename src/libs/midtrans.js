const midtransClient = require('midtrans-client')
const midtransCOnnectionConfig = require('../../config/midtrans')

const environment = process.env.NODE_ENV

const midtransCoreApi = new midtransClient.CoreApi(midtransCOnnectionConfig[environment])

module.exports = midtransCoreApi