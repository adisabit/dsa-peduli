const HttpError = require('./http')

class DataNotFoundError extends HttpError {
    constructor(statusCode, message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

module.exports = DataNotFoundError