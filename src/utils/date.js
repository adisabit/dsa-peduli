/**
 * Format date time
 * 
 * @param {Date} dateTime 
 */
const formatDateTime = function(dateTime) {
    const months = [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ]
    
    const newDateTime = new Date(dateTime)

    const date = newDateTime.getDate() < 10 ? "0" + newDateTime.getDate() : newDateTime.getDate(); 
    const minutes = newDateTime.getMinutes() < 10 ? "0" + newDateTime.getMinutes() : newDateTime.getMinutes(); 
    const hours = newDateTime.getHours() < 10 ? "0" + newDateTime.getHours() : newDateTime.getHours(); 

    const formattedDateTime = date + " " + months[newDateTime.getMonth()] + " " + newDateTime.getFullYear() + " " + hours + ":" + minutes

    return formattedDateTime
}

module.exports = { formatDateTime }