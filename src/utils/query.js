const ignoredArray = ['createdAt', 'updatedAt', 'deletedAt']

const filterCreated = (data) => {
    const arrayKeys = Object.keys(data.dataValues)

    const filtered = arrayKeys.filter((value) => {
        return !ignoredArray.includes(value)
    })

    let result = {}

    filtered.forEach(filteredItem => {
        result[filteredItem] = data[filteredItem]
    })

    return result
}

module.exports = { filterCreated }