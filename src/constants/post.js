const constants = {
    STATUS_PUBLISHED: 'PUBLISHED',
    STATUS_DRAFT: 'DRAFT',
    CATEGORY_NEWS: 'NEWS',
    CATEGORY_EDUCATION: 'EDUCATION'
}

module.exports = constants