const constants = {
    SECTION_STATISTIC: "STATISTIC",
    SECTION_ABOUT_US: "ABOUT_US",
    SECTION_LINK: "LINK"
}

module.exports = constants