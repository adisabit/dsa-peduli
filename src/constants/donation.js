require('dotenv').config()

const env = process.env

const constants = {
    CATEGORY_GENERAL: 'GENERAL',
    CATEGORY_FIDYAH: 'FIDYAH',
    CATEGORY_5K_MOVEMENT: '5K_MOVEMENT',
    STATUS_NEW: 'NEW',
    STATUS_PAID: 'PAID',
    STATUS_PENDING: 'PENDING',
    STATUS_CHALLENGE: 'CHALLENGE',
    STATUS_FAILED: 'FAILED',
    LATEST_DONATION_NUMBER: env.LATEST_DONATION_NUMBER || 10
}

module.exports = constants