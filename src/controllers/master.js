const models = require('../models/index')
const MasterPaymentMethod = models['MasterPaymentMethod']
const masterConstant = require('../constants/master')


const getMasterPaymentMethod = async () => {
    const result = await MasterPaymentMethod.findAll({
        attributes: ['id', 'name', 'description', 'imageUrl'],
        where: { type: masterConstant.TYPE_EXTERNAL }
    })

    return result;
}


module.exports = {
    getMasterPaymentMethod
}