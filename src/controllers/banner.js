const models = require('../models/index')
const Banner = models['Banner']


/**
 * Get active banners
 */
const getActiveBanners = async () => {
    const banners = await Banner.findAll({
        where: { isActive: true },
        attributes: ['name', 'description', 'imageUrl', 'destinationUrl'],
        order: [
            ['createdAt', 'DESC']
        ]
    })

    return banners
}


module.exports = {
    getActiveBanners
}