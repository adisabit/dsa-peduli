const models = require('../models/index');
const Section = models['Section']
const camelCase = require('camelcase')


const getSectionByCategory = async (category) => {
    const sections = await Section.findAll({
        where: {
            category: category,
            isActive: true
        },
        attributes: ['code', 'name', 'value']
    })

    const result = {}

    for (i = 0; i < sections.length; i++) {
        const key = camelCase(sections[i].code)
        
        result[key] = {
            name: sections[i].name,
            value: sections[i].value
        }
    }

    return result
}


module.exports = { getSectionByCategory }