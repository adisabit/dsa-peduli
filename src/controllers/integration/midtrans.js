const models = require('../../models')
const moment = require('moment')
const uuidv4 = require('uuid/v4')
const HttpError = require('../../errors/http')
const midtransCoreApi = require('../../libs/midtrans')
const IntegrationMidtransLog = models['IntegrationMidtransLog']
const IntegrationMidtransCharge = models['IntegrationMidtransCharge']
const Donation = models['Donation']
const donationConstant = require('../../constants/donation')


const insertLog = async (req) => {
    const data = {
        url: req.hostname,
        requestUrl: req.originalUrl,
        requestMethod: req.method,
        requestHeader: JSON.stringify(req.headers),
        requestBody: JSON.stringify(req.body),
        requestTime: moment().format()
    }

    const result = await IntegrationMidtransLog.create(data)

    return result
}

const updateLog = async (logId, res) => {
    const log = IntegrationMidtransLog.findOne({
        where: {id: logId}
    })

    const memoryUsage = process.memoryUsage().heapUsed / 1024 / 1024
    const now = moment(new Date())

    const data = {
        serverMemoryUsage: memoryUsage,
        responseCode: res.status_code,
        responseBody: JSON.stringify(res),
        responseTime: now
    }

    const result = await IntegrationMidtransLog.update(data, {
        where: {id: logId}
    })

    return result
}

const generateFirstLastName = (name) => {
    const splitted = name.split(' ')
    const firstName = splitted[0]
    const lastName = splitted.slice(1).join(' ')

    return {
        firstName: firstName,
        lastName: lastName
    }
}

const charge = async (req, donationId, category, paymentType, channelName, name, amount) => {
    const log = await insertLog(req)

    const orderId = 'donation_' + donationId + '_' + uuidv4()
    const customerName = generateFirstLastName(name)  

    const params = {
        payment_type: paymentType,
        transaction_details: {
            order_id: orderId,
            gross_amount: amount
        },
        item_details: [
            {
                id: donationId,
                price: amount,
                quantity: 1,
                name: 'Donasi ' + category.toLowerCase().replace('_', ' '),
            }
        ],
        customer_details: {
            first_name: customerName.firstName,
            last_name: customerName.lastName
        }
    }

    // Need some consideration
    if (paymentType.toLowerCase() === 'bank_transfer') {
        params.bank_transfer = {
            bank: channelName
        }
    } else if (paymentType.toLowerCase() === 'gopay') {
        // temporary empty params
        params.gopay = {}
    } else if (paymentType.toLowerCase() == 'echannel') {
        params.echannel = {
            bill_info1: 'Pembayaran untuk:',
            bill_info2: 'donasi'
        }
    }

    try {
        const response = await midtransCoreApi.charge(params)
    
        await updateLog(log.id, response)
        
        const chargeData = {
            transactionId: response.transaction_id,
            orderId: response.order_id,
            merchantId: response.merchant_id,
            grossAmount: response.gross_amount,
            currency: response.currency,
            paymentType: response.payment_type,
            transactionTime: response.transaction_time,
            transactionStatus: response.transaction_status,
            fraudStatus: response.fraud_status
        }

        if (paymentType.toLowerCase() === 'bank_transfer') {
            chargeData.payments = JSON.stringify(response.va_numbers)
        } else if (paymentType.toLowerCase() === 'gopay') {
            // temporary empty params
            chargeData.payments = JSON.stringify(response.actions)
        } else if (paymentType.toLowerCase() == 'echannel') {
            chargeData.payments = JSON.stringify({
                bill_key: response.bill_key,
                biller_code: response.biller_code
            })
        }
    
        const result = await IntegrationMidtransCharge.create(chargeData)
    
        return result
    } catch (error) {
        console.log(error)
        throw new HttpError(500, 'Something went wrong!')
    }
}

const handleNotification = async (notificationJson) => {
    const statusResponse = await midtransCoreApi.transaction.notification(notificationJson)

    const orderId = statusResponse.order_id
    const transactionStatus = statusResponse.transaction_status
    const fraudStatus = statusResponse.fraud_status

    const donationId = parseInt(orderId.split('_')[1])
    
    let result

    if (transactionStatus == 'capture') {
        if (fraudStatus == 'challenge'){
            result = await Donation.update({
                status: donationConstant.STATUS_CHALLENGE
            }, {
                where: { id: donationId }
            })
        } else if (fraudStatus == 'accept') {
            result = await Donation.update({
                status: donationConstant.STATUS_PAID
            }, {
                where: { id: donationId }
            })
        }
    } else if (transactionStatus == 'cancel' || transactionStatus == 'deny' || transactionStatus == 'expire'){
        result = await Donation.update({
            status: donationConstant.STATUS_FAILED
        }, {
            where: { id: donationId }
        })
    } else if (transactionStatus == 'pending'){
        result = await Donation.update({
            status: donationConstant.STATUS_PENDING
        }, {
            where: { id: donationId }
        })
    } else if (transactionStatus == 'settlement') {
        result = await Donation.update({
            status: donationConstant.STATUS_PAID
        }, {
            where: { id: donationId }
        })
    } else {
        result = 0
    }

    return result
}


module.exports = {
    charge,
    handleNotification
}