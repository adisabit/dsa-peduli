const models = require('../models/index')
const Campaign = models['Campaign']
const Donation = models['Donation']
const moment = require('moment')
const donationConstant = require('../constants/donation')
const DataNotFoundError = require('../errors/data-not-found')


/**
 * Get latest campaigns
 */
const getLatestCampaings = async () => {
    const campaigns = await Campaign.findAll({
        attributes: ["id", 'category', "featuredImageUrl", "title", "body", "targetAmount", "targetDate"],
        order: [
            ['createdAt', 'DESC']
        ]
    })

    for (i = 0; i < campaigns.length; i++) {
        campaigns[i].dataValues.donations = await Donation.findAll({
            where: {
                campaignId: campaigns[i].dataValues.id,
                status: donationConstant.STATUS_PAID
            }
        })

        let total = 0        
        
        for (j = 0; j < campaigns[i].dataValues.donations.length; j++) {
            total += campaigns[i].dataValues.donations[j].amount
        }
        
        campaigns[i].dataValues.amountCollected = total

        delete campaigns[i].dataValues.donations
    }


    return campaigns
}

/**
 * Get campaign by its id
 * 
 * @param {BigInt} id 
 */
const getById = async (id) => {
    const campaign = await Campaign.findOne({
        where: { id: id },
        attributes: ["id", 'category', "featuredImageUrl", "title", "body", "targetAmount", "targetDate"]
    })

    if (!campaign) {
        throw new DataNotFoundError(204, 'Campaign tidak ditemukan!')
    }

    campaign.dataValues.donations = await Donation.findAll({
        where: {
            campaignId: campaign.dataValues.id,
            status: donationConstant.STATUS_PAID
        }
    })

    campaign.dataValues.targetDate = moment(campaign.dataValues.targetDate).format()

    let total = 0

    for (i = 0; i < campaign.dataValues.donations.length; i++) {
        total += campaign.dataValues.donations[i].dataValues.amount

        campaign.dataValues.donations[i].dataValues = {
            fullName: campaign.dataValues.donations[i].dataValues.isAnonymous ? 'Hamba Allah' : campaign.dataValues.donations[i].dataValues.fullName,
            donationTime: campaign.dataValues.donations[i].dataValues.donationTime = moment(campaign.dataValues.donations[i].dataValues.donationTime).format(),
            amount: campaign.dataValues.donations[i].dataValues.amount
        }
    }

    campaign.dataValues.amountCollected = total

    return campaign
}

/**
 * Donate to campaign
 * 
 * @param {BigInt} campaignId 
 * @param {Object} data 
 */
const donate = async (campaignId, data) => {
    const result = await Donation.create({
        campaignId: campaignId,
        paymentMethodId: data.paymentMethodId,
        category: donationConstant.CATEGORY_GENERAL,
        fullName: data.fullName,
        phoneNumber: data.phoneNumber,
        amount: data.amount,
        isAnonymous: data.isAnonymous,
        status: donationConstant.STATUS_NEW
    })

    return result
}

module.exports = {
    getLatestCampaings,
    getById,
    donate
}