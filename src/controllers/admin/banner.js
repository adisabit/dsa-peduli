const models = require('../../models/index')
const Banner = models['Banner']
const { v4: uuidv4 } = require('uuid')
const storage = require('../../libs/storage')
const DataNotFoundError = require('../../errors/data-not-found')


/**
 * Create new banner
 * 
 * @param {Object} req 
 */
const create = async (req) => {
    let imageUrl = null
    let data = req.body

    if (req.files) {
        const bannerImage = req.files.bannerImage
        const fileType = bannerImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `banner/${fileName}`

        try {
            imageUrl = await storage.upload(bannerImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: bannerImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }

    data.imageUrl = imageUrl
    data.isActive = data.isActive != undefined ? data.isActive : true

    const result = await Banner.create(data)

    return result
}

/**
 * Get all banners
 */
const list = async () => {
    const banners = await Banner.findAll({ 
        order: [
            ['createdAt', 'DESC']
        ],
    })

    return banners
}

/**
 * Get banner detail by id
 * 
 * @param {BigInt} id 
 */
const detail = async (id) => {
    const banner = await Banner.findOne({
        where: { id: id }
    })

    if (!banner) {
        throw new DataNotFoundError(204, 'Banner tidak ditemukan!')
    }

    return banner
}

/**
 * Update banner data
 * 
 * @param {BigInt} id banner id
 * @param {Object} req banner new data
 */
const update = async (id, req) => {
    const banner = await Banner.findOne({
        where: { id: id }
    })

    if (!banner) {
        throw new DataNotFoundError(204, 'Banner tidak ditemukan!')
    }

    let imageUrl = banner.imageUrl
    const data = req.body

    if (req.files) {
        const bannerImage = req.files.bannerImage
        const fileType = bannerImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `banner/${fileName}`

        try {
            imageUrl = await storage.upload(bannerImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: bannerImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }

    data.imageUrl = imageUrl

    const result = await Banner.update(data, {
        where: { id: id }
    })

    return {
        totalUpdated: result[0]
    }
}

/**
 * Delete banner
 * 
 * @param {BigInt} id the banner id 
 */
const deleteById = async (id) => {
    const result = await Banner.destroy({
        where: { id: id }
    })

    return {
        totalDeleted: result
    }
}

module.exports = {
    create,
    list,
    detail,
    update,
    deleteById
}