const models = require('../../models/index');
const MasterPaymentMethod = models['MasterPaymentMethod']
const { getDonationSummary } = require('../donation')
const moment = require('moment')


const getLatestDonationSummaries = async (days = 10) => {
    const query = "select cast(donationTime as date) as donationDate, count(donationTime) as totalDonation from donation where donationTime > :donationTime group by donationDate"
    const date = moment().subtract(days, 'days').format("YYYY-MM-DD")

    const result = await models.sequelize.query(query, {
        replacements: { donationTime: date },
        type: models.sequelize.QueryTypes.SELECT
    })

    return result
}

const getTotalDonationPerCategory = async () => {
    const query = "select category, count(category) as totalDonation from donation group by category"

    const results = await models.sequelize.query(query, {
        type: models.sequelize.QueryTypes.SELECT
    })

    const categoryMap = {
        "GENERAL": "Non Kategori",
        "FIDYAH": "Fidyah",
        "5K_MOVEMENT": "5K Movement"
    }

    results.forEach((result, i) => {
        results[i].category = categoryMap[result.category]
    })

    return results
}

const getTotalDonationPerPaymentMethod = async () => {
    const query = "select paymentMethodId as paymentMethod, count(paymentMethodId) as totalDonation from donation group by paymentMethodId"

    const results = await models.sequelize.query(query, {
        type: models.sequelize.QueryTypes.SELECT
    })

    results.forEach(async (result, i) => {
        const paymentMethod = await MasterPaymentMethod.findByPk(result.paymentMethod, {
            attributes: ['name']
        })

        results[i].paymentMethod = paymentMethod ? paymentMethod.name : null
    })

    return results
}

const adminGetDonationSummary = async () => {
    const donationSummary = await getDonationSummary()

    return donationSummary
}

const getRecap = async (days = 10) => {
    const latestDonationSummaries = await getLatestDonationSummaries(days)
    const totalDonationPerCategory = await getTotalDonationPerCategory()
    const totalDonationPerPaymentMethod = await getTotalDonationPerPaymentMethod()
    const donationSummary = await adminGetDonationSummary()

    return {
        totalDonation: donationSummary.total,
        latestDonationSummaries: latestDonationSummaries,
        donationPerCategory: totalDonationPerCategory,
        donationPerPaymentMethod: totalDonationPerPaymentMethod,
        donationSummary: donationSummary.summaries,
    }
}


module.exports = {
    getRecap
}