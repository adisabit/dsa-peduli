const models = require('../../models/index');
const Section = models['Section']


const createSection = async (req) => {
    const data = {
        code: req.code,
        category: req.category,
        name: req.name,
        value: req.value
    }

    const result = await Section.create(data)

    return result
}

const updateSection = async (req) => {
    const id = req.id
    const data = {
        category: req.category,
        name: req.name,
        value: req.value
    }

    const result = await Section.update(data, {
        where: { id: id }
    })

    return {
        totalUpdated: result[0]
    }
}

const deleteSection = async (id) => {
    const result = await Section.destroy({
        where: { id: id }
    })

    return {
        totalDeleted: result
    }
}

const getSectionByCategory = async (category) => {
    const section = await Section.findAll({
        where: { category: category }
    })

    return section
}

const getSection = async (category) => {
    if (!category) {
        const sections = await Section.findAll({
            attributes: [
                [models.Sequelize.fn('DISTINCT', models.Sequelize.col('category')), 'category']
            ]
        })

        let results = []
        
        for (i = 0; i < sections.length; i++) {
            let result = {}
            const sectionItem = await getSectionByCategory(sections[i].category)
            
            result[sections[i].category] = sectionItem
            results.push(result)
        }

        return results
    } else {        
        const result = await getSectionByCategory(category)

        return result
    }

}


module.exports = {
    createSection,
    updateSection,
    deleteSection,
    getSection
}