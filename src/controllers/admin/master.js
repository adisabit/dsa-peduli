const models = require('../../models/index')
const MasterPaymentMethod = models['MasterPaymentMethod']
const DataNotFoundError = require('../../errors/data-not-found')


/**
 * Create new master payment method
 * 
 * @param {Object} req request data
 */
const create = async (req) => {
    const masterPaymentMethod = await MasterPaymentMethod.create(req)

    return masterPaymentMethod
}

/**
 * Update master payment method
 * 
 * @param {BigInt} id the id
 * @param {Object} req request data
 */
const update = async (id, req) => {
    const masterPaymentMethod = await MasterPaymentMethod.update(req, {
        where: { id: id }
    })

    return {
        totalUpdated: masterPaymentMethod[0]
    }
}

/**
 * Delete by id
 * 
 * @param {BigInt} id the id
 */
const deleteById = async (id) => {
    const masterPaymentMethod = await MasterPaymentMethod.destroy({
        where: { id: id }
    })

    return {
        totalDeleted: masterPaymentMethod
    }
}

/**
 * Get all master payment method
 */
const list =  async () => {
    return await MasterPaymentMethod.findAll({
        attributes: ['id', 'type', 'name', 'description', 'imageUrl', 'accountNumber', 'accountName']
    })
}

/**
 * Get master payment method detail
 * 
 * @param {BigInt} id the id
 */
const detail = async (id) => {
    const masterPaymentMethod = await MasterPaymentMethod.findOne({
        where: { id: id }
    })

    if (!masterPaymentMethod) {
        throw new DataNotFoundError(204, 'Metode pembayaran tidak ditemukan!')
    }

    return masterPaymentMethod
}

module.exports = {
    create,
    update,
    deleteById,
    list,
    detail
}