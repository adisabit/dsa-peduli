const models = require('../../models/index')
const Post = models['Post']
const storage = require ('../../libs/storage')
const { v4: uuidv4 } = require('uuid')
const constant = require('../../constants/post')
const moment = require('moment')
const DataNotFoundError = require('../../errors/data-not-found')


/**
 * Create post
 * 
 * @param {Object} req 
 * @return {Object}
 */
const create = async (req) => {
    let featuredImageUrl = null
    const data = req.body

    if (req.files) {
        const featureImage = req.files.featureImage
        const fileType = featureImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `post/${fileName}`

        try {
            featuredImageUrl = await storage.upload(featureImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: featureImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }

    const status = data.status ? data.status : constant.STATUS_PUBLISHED
 
    const result = await Post.create({
        title: data.title,
        category: data.category,
        body: data.body,
        featuredImageUrl: featuredImageUrl,
        status: status,
        publishedDate: status === constant.STATUS_PUBLISHED ? moment().format() : null
    })

    return result
}

/**
 * Publish post 
 * 
 * @param {int} id 
 */
const publish = async (id, category) => {
    const result = await Post.update({
        status: constant.STATUS_PUBLISHED,
        publishedDate: moment().format()
    }, {
        where: {
            id: id,
            category: category
        }
    })

    return {
        totalPublished: result[0]
    }
}

/**
 * Update post
 * 
 * @param {int} id 
 * @param {Object} req 
 * @return {Object}
 */
const update = async (id, category, req) => {
    const post = await Post.findOne({
        where: {
            id: id,
            category: category
        },
        attributes: ['featuredImageUrl']
    })

    if (!post) {
        throw new DataNotFoundError(204, 'Data tidak ditemukan!')
    }

    let featuredImageUrl = post.featuredImageUrl
    const data = req.body

    if (req.files) {
        const featureImage = req.files.featureImage
        const fileType = featureImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `post/${fileName}`

        try {
            featuredImageUrl = await storage.upload(featureImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: featureImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }

    const result = await Post.update({
        title: data.title,
        body: data.body,
        featuredImageUrl: featuredImageUrl,
    }, {
        where: { id: id }
    })

    return {
        totalUpdated: result[0]
    }
}

/**
 * Delete post by id
 *  
 * @param {int} id 
 * @return {Object}
 */
const deleteById = async (id, category) => {
    const result = await Post.destroy({
        where: {
            id: id,
            category: category
        }
    });

    return {
        totalDeleted: result
    }
}

/**
 * Calculate limit and offset 
 * 
 * @param {int} page 
 * @param {int} itemPerPage 
 */
const calculateLimitOffset = (page, itemPerPage) => {
    const limit = itemPerPage
    const offset = (page - 1) * limit

    return {
        limit: limit,
        offset: offset
    }
}

/**
 * Check if page exist
 * 
 * @param {int} limit 
 * @param {int} offset 
 * @param {string} status 
 */
const hasPage = async (category, status, limit, offset) => {
    if (offset < 0) {
        return false
    }

    const post = await Post.findAll({
        where: { 
            category: category,
            status: status
        },
        attributes: ['featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
        order: [
            ['publishedDate', 'DESC']
        ],
        limit: limit,
        offset: offset
    })

    if (post.length == 0) {
        return false
    }

    return true
}

/**
 * Get post
 * 
 * @param {string} status 
 * @param {int} limit 
 * @param {int} offset 
 */
const getPost = async (category, status, limit, offset) => {
    if (category && status && limit && offset) {
        return await Post.findAll({
            where: { 
                status: status,
                category: category
            },
            attributes: ['id', 'featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
            order: [
                ['publishedDate', 'DESC']
            ],
            limit: limit,
            offset: offset
        })
    } else if (category && status) {
        return await Post.findAll({
            where: {
                category: category,
                status: status
            },
            attributes: ['id', 'featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
            order: [
                ['publishedDate', 'DESC']
            ]
        })
    } else if (category) {
        return await Post.findAll({
            where: { category: category },
            attributes: ['id', 'featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
            order: [
                ['publishedDate', 'DESC']
            ]
        })
    } else {
        return await Post.findAll({
            attributes: ['id', 'featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
            order: [
                ['publishedDate', 'DESC']
            ]
        })
    }
}

/**
 * Get prev and next page url
 * 
 * @param {string} originalUrl 
 * @param {int} page 
 * @param {int} itemPerPage 
 */
const getPageUrl = async (originalUrl, category, status, page, itemPerPage) => {
    let nextPageUrl
    const nextPagination = calculateLimitOffset(page + 1, itemPerPage)
    
    let prevPageUrl
    const prevPagination = calculateLimitOffset(page - 1, itemPerPage)
    
    if ( await hasPage(category, status, nextPagination.limit, nextPagination.offset) ) {
        nextPageUrl = `${originalUrl}?page=${page + 1}&itemPerPage=${itemPerPage}`
    } else {
        nextPageUrl = null
    }

    if ( await hasPage(category, status, prevPagination.limit, prevPagination.offset) ) {
        prevPageUrl = `${originalUrl}?page=${page - 1}&itemPerPage=${itemPerPage}`
    } else {
        prevPageUrl = null
    }

    return {
        prevPageUrl: prevPageUrl,
        nextPageUrl: nextPageUrl
    }
}

/**
 * Get post with status
 * 
 * @param {string} originalUrl 
 * @param {int} page 
 * @param {int} itemPerPage 
 * @param {string} status 
 */
const getPostWithStatus = async (originalUrl, page, itemPerPage, category, status) => {
    const pagination = calculateLimitOffset(page, itemPerPage)
    
    const post = await getPost(category, status, pagination.limit, pagination.offset)
    const allPost = await getPost(category, status)
    const pageUrl = await getPageUrl(originalUrl, category, status, page, itemPerPage)

    const result =  {
        prevPageUrl: pageUrl.prevPageUrl,
        nextPageUrl: pageUrl.nextPageUrl,
        total: allPost.length
    }

    result[category.toLowerCase()] = post

    return result
} 

/**
 * Get all published post
 * 
 * @return {Object}
 */
const getPublished = async (originalUrl, category, page, itemPerPage) => {
    return await getPostWithStatus(originalUrl, page, itemPerPage, category, constant.STATUS_PUBLISHED)
}

/**
 * Get all draft post
 * 
 * @return {Object}
 */
const getDraft = async (originalUrl, category, page, itemPerPage) => {
    return await getPostWithStatus(originalUrl, page, itemPerPage, category, constant.STATUS_DRAFT)
}

/**
 * Get all post
 */
const getAll = async (category) => {
    return {
        published: await getPost(category, constant.STATUS_PUBLISHED),
        draft: await getPost(category, constant.STATUS_DRAFT)
    }
}

/**
 * Get post by id
 * 
 * @param {BigInt} id 
 */
const getById = async (id, category) => {
    const post = await Post.findOne({
        where: {
            id: id,
            category: category
        }
    })

    if (!post) {
        throw new DataNotFoundError(204, 'Data tidak ditemukan!')
    }
    
    return post
}

module.exports = {
    create,
    publish,
    update,
    deleteById,
    getPublished,
    getDraft,
    getAll,
    getById
}