const models = require('../../models/index')
const Campaign = models['Campaign']
const Donation = models['Donation']
const { v4: uuidv4 } = require('uuid')
const storage = require ('../../libs/storage')
const moment = require('moment')
const DataNotFoundError = require('../../errors/data-not-found')
const donationConstant = require('../../constants/donation')


/**
 * Create new campaign
 * 
 * @param {Object} req 
 */
const create = async (req) => {
    let featuredImageUrl = null
    const data = req.body

    if (req.files) {
        const featureImage = req.files.featureImage
        const fileType = featureImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `campaign/${fileName}`

        try {
            featuredImageUrl = await storage.upload(featureImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: featureImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }
 
    const result = await Campaign.create({
        category: data.category,
        title: data.title,
        body: data.body,
        featuredImageUrl: featuredImageUrl,
        targetDate: data.targetDate,
        targetAmount: data.targetAmount
    })

    return result
}

/**
 * Update campaign
 * 
 * @param {BigInt} id 
 * @param {Object} data 
 */
const update = async (id, req) => {
    const news = await Campaign.findOne({
        where: { id: id },
        attributes: ['featuredImageUrl']
    })

    if (!news) {
        throw new DataNotFoundError(204, 'Campaign tidak ditemukan!')
    }

    let featuredImageUrl = news.featuredImageUrl
    const data = req.body

    if (req.files) {
        const featureImage = req.files.featureImage
        const fileType = featureImage.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        const destination = `campaign/${fileName}`

        try {
            featuredImageUrl = await storage.upload(featureImage.tempFilePath, {
                public: true,
                destination: destination,
                metadata: {
                    contentType: featureImage.mimetype,
                },
                gzip: true
            })
        } catch (error) {
            throw error
        }
    }

    const result = await Campaign.update({
        title: data.title,
        body: data.body,
        targetAmount: data.targetAmount,
        targetDate: data.targetDate,
        featuredImageUrl: featuredImageUrl
    }, {
        where: { id: id }
    })

    return {
        totalUpdated: result[0]
    }
}

/**
 * Delete by id 
 * 
 * @param {BigInt} id 
 */
const deleteById = async (id) => {
    const result = await Campaign.destroy({
        where: { id: id }
    });

    return {
        totalDeleted: result
    }
}

/**
 * Get campaign by id
 * 
 * @param {BigInt} id 
 */
const getById = async (id) => {
    const campaign = await Campaign.findOne({
        where: { id: id },
        attributes: ["id", "featuredImageUrl", "title", "body", "targetAmount", "targetDate"],
        include: 'donations'
    })

    if (!campaign) {
        throw new DataNotFoundError(204, 'Campaign tidak ditemukan!')
    }

    campaign.dataValues.targetDate = moment(campaign.dataValues.targetDate).format()

    let total = 0

    for (i = 0; i < campaign.donations.length; i++) {
        if (campaign.donations[i].status == donationConstant.STATUS_PAID) {
            total += campaign.donations[i].amount
        } 
        
        campaign.donations[i].dataValues.donationTime = moment(campaign.donations[i].dataValues.donationTime).format()
    }

    campaign.dataValues.amountCollected = total

    return campaign
}

/**
 * Get all campaign
 */
const getAll = async () => {
    const campaigns = await Campaign.findAll({
        order: [ ['createdAt', 'DESC'] ],
        include: 'donations'
    })

    for (i = 0; i < campaigns.length; i++) {
        let total = 0        
        
        for (j = 0; j < campaigns[i].donations.length; j++) {
            if (campaigns[i].donations[j].status == donationConstant.STATUS_PAID) {
                total += campaigns[i].donations[j].amount
            }
        }
        
        campaigns[i].dataValues.amountCollected = total
        campaigns[i].dataValues.targetDate = moment(campaigns[i].dataValues.targetDate).format()

        delete campaigns[i].dataValues.donations
    }

    return campaigns
}


module.exports = {
    create,
    update,
    deleteById,
    getById,
    getAll
}