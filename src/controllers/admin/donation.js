const models = require('../../models/index')
const Donation = models['Donation']
const DonationFidyah = models['DonationFidyah']
const DonationFiveThousand = models['DonationFiveThousand']
const dateUtils = require('../../utils/date')
const donationConstants = require('../../constants/donation')
const DataNotFoundError = require('../../errors/data-not-found')
const nodeExcel = require('excel-export')
const { Op } = require('sequelize')
const moment = require('moment')


/**
 * Get all new donations
 */
const getNewDonations = async () => {
    // Get latest donation
    const donations = await Donation.findAll({
        attributes: ['id', 'category', 'paymentMethod', 'donationTime', 'fullName', 'amount', 'isAnonymous'],
        where: { status: donationConstants.STATUS_NEW },
        order: [ ['donationTime', 'DESC'] ]
    });

    // Manipulate donations
    result = [];
    donations.forEach((donation, i) => {
        const formattedDonationTime = dateUtils.formatDateTime(donation.donationTime)

        donation.dataValues.donationTime = formattedDonationTime

        result.push(donation)
    });

    // return result
    return result
}

/**
 * Get new donation by id
 * 
 * @param {int} id 
 */
const getNewDonationById = async (id) => {
    const donation = await Donation.findOne({
        where: {
            id: id,
            status: donationConstants.STATUS_NEW
        },
        attributes: ['id', 'category', 'paymentMethod', 'donationTime', 'fullName', 'amount', 'isAnonymous']
    })

    if (!donation) {
        throw new DataNotFoundError(204, "Data donasi tidak ditemukan!")
    } 

    donation.dataValues.donationTime = dateUtils.formatDateTime(donation.dataValues.donationTime)

    return donation
}

/**
 * Bulk verify donation
 * 
 * @param {array} donations 
 */
const verify = async (donations) => {
    donations.forEach(async donation => {
        const result = await Donation.update({
            status: donationConstants.STATUS_PAID
        }, {
            where: { id: donation }
        })

        total += result[0]
    })

    return {
        totalUpdated: donations.length
    }
}

/**
 * Create new donation
 * 
 * @param {Object} req 
 */
const create =  async (req) => {
    const generalData = {
        paymentMethodId: req.paymentMethodId,
        paymentMethod: req.paymentMethod,
        category: req.category,
        fullName: req.fullName,
        amount: req.amount,
        isAnonymous: req.isAnonymous,
        status: req.status
    }

    const donation = await Donation.create(generalData)

    let detail
    if (req.category === donationConstants.CATEGORY_FIDYAH) {
        detail = {
            donationId: donation.id,
            address: req.address,
            days: req.days
        }

        let detailResult = await DonationFidyah.create(detail)
        
        donation.dataValues.address = detailResult.dataValues.address
        donation.dataValues.days = detailResult.dataValues.days
    } else if (req.category === donationConstants.CATEGORY_5K_MOVEMENT) {
        detail = {
            donationId: donation.id,
            age: req.age,
            domicile: req.domicile
        }

        let detailResult = await DonationFiveThousand.create(detail)
        donation.dataValues.age = detailResult.dataValues.age
        donation.dataValues.domicile = detailResult.dataValues.domicile
    }

    return donation
}

/**
 * Update donation
 * 
 * @param {integer} id 
 * @param {Object} req 
 */
const update =  async (id, req) => {
    const generalData = {
        paymentMethodId: req.paymentMethodId,
        paymentMethod: req.paymentMethod,
        category: req.category,
        fullName: req.fullName,
        amount: req.amount,
        isAnonymous: req.isAnonymous,
        status: req.status
    }

    const result = await Donation.update(generalData, {
        where: { id: id }
    })

    let detail
    if (req.category === donationConstants.CATEGORY_FIDYAH) {
        detail = {
            address: req.address,
            days: req.days
        }

        await DonationFidyah.update(detail, {
            where: { donationId: id }
        })
    } else if (req.category === donationConstants.CATEGORY_5K_MOVEMENT) {
        detail = {
            age: req.age,
            domicile: req.domicile
        }

        DonationFiveThousand.update(detail, {
            where: { donationId: id }
        })
    }

    return {
        totalUpdated: result[0]
    }
}

/**
 * Delete by id
 * 
 * @param {integer} id 
 */
const deleteById = async (id) => {
    const donation = await Donation.findOne({
        where : { id: id }
    });

    const result = await Donation.destroy({
        where : { id: id }
    })

    if (donation) {
        if (donation.category === donationConstants.CATEGORY_FIDYAH) {
            await DonationFidyah.destroy(detail, {
                where: { donationId: id }
            })
        } else if (donation.category === donationConstants.CATEGORY_5K_MOVEMENT) {
            DonationFiveThousand.destroy(detail, {
                where: { donationId: id }
            })
        }
    }

    return {
        totalDeleted: result
    }
}

/**
 * Calculate limit and offset 
 * 
 * @param {int} page 
 * @param {int} itemPerPage 
 */
const calculateLimitOffset = (page, itemPerPage) => {
    const limit = itemPerPage
    const offset = (page - 1) * limit

    return {
        limit: limit,
        offset: offset
    }
}

/**
 * Check if page exist
 * 
 * @param {int} limit 
 * @param {int} offset 
 */
const hasPage = async (limit, offset) => {
    if (offset < 0) {
        return false
    }

    const news = await Donation.findAll({
        order: [
            ['donationTime', 'DESC']
        ],
        limit: limit,
        offset: offset
    })

    if (news.length == 0) {
        return false
    }

    return true
}

/**
 * Get prev and next page url
 * 
 * @param {string} originalUrl 
 * @param {int} page 
 * @param {int} itemPerPage 
 */
const getPageUrl = async (originalUrl, page, itemPerPage) => {
    let nextPageUrl
    const nextPagination = calculateLimitOffset(page + 1, itemPerPage)
    
    let prevPageUrl
    const prevPagination = calculateLimitOffset(page - 1, itemPerPage)
    
    if ( await hasPage(nextPagination.limit, nextPagination.offset) ) {
        nextPageUrl = `${originalUrl}?page=${page + 1}&itemPerPage=${itemPerPage}`
    } else {
        nextPageUrl = null
    }

    if ( await hasPage(prevPagination.limit, prevPagination.offset) ) {
        prevPageUrl = `${originalUrl}?page=${page - 1}&itemPerPage=${itemPerPage}`
    } else {
        prevPageUrl = null
    }

    return {
        prevPageUrl: prevPageUrl,
        nextPageUrl: nextPageUrl
    }
}

/**
 * Get all donations 
 * 
 * @param {string} originalUrl 
 * @param {int} page 
 * @param {int} itemPerPage 
 */
const getAll = async (originalUrl, page, itemPerPage) => {
    const pagination = calculateLimitOffset(page, itemPerPage)

    const donations = await Donation.findAll({
        attributes: ['id', 'category', 'paymentMethod', 'donationTime', 'fullName', 'amount', 'isAnonymous', 'status'],
        order: [
            ['donationTime', 'DESC']
        ],
        limit: pagination.limit,
        offset: pagination.offset,
    });

    const allDonations = await Donation.findAll({
        order: [
            ['donationTime', 'DESC']
        ],
    });

    const pageUrl = await getPageUrl(originalUrl, page, itemPerPage)

    return {
        donations: donations,
        prevPageUrl: pageUrl.prevPageUrl,
        nextPageUrl: pageUrl.nextPageUrl,
        total: allDonations.length
    }
}

/**
 * Export donation data
 * 
 * @param {Date} startDate start date
 * @param {Date} endDate end date
 */
const exportData = async (startDate, endDate) => {
    const config = {}

    config.name = 'Donasi'
    config.cols = [{
        caption: 'Tanggal',
        type: 'string'
    }, {
        caption: 'Kategori',
        type: 'string',
        beforeCellWrite: function(row, cellData) {
            if (cellData.toUpperCase() == donationConstants.CATEGORY_GENERAL) {
                return 'Donasi Umum'
            } else if (cellData.toUpperCase() == donationConstants.CATEGORY_FIDYAH) {
                return 'Fidyah'
            } else if (cellData.toUpperCase() == donationConstants.CATEGORY_5K_MOVEMENT) {
                return '5K Movement'
            } else {
                return ''
            }
        }
    }, {
        caption: 'Nama',
        type: 'string'
    }, {
        caption: 'Nominal',
        type: 'string'
    }, {
        caption: 'Bayar Via',
        type: 'string'
    }]
    config.rows = []

    let donations

    if (startDate && endDate) {
        donations = await Donation.findAll({
            where: {
                donationTime: {
                    [Op.between]: [moment(startDate).format(), moment(endDate).format()]
                }
            },
        })
    } else if (startDate && !endDate) {
        donations = await Donation.findAll({
            where: {
                donationTime: {
                    [Op.between]: [moment(startDate).format(), moment().format()]
                }
            },
        })
    } else if (!startDate && endDate) {
        donations = await Donation.findAll({
            where: {
                donationTime: {
                    [Op.between]: [moment(endDate).startOf('month').format(), moment(endDate).format()]
                }
            },
        })
    } else {
        donations = await Donation.findAll({
        })
    }

    for (let donation of donations) {
        config.rows.push([
            moment(donation.donationTime).format("DD-MM-YYYY"), donation.category, donation.fullName, donation.amount, donation.paymentMethod
        ])
    }

    return {
        excelFile: nodeExcel.execute(config),
        fileName: 'donation-' + moment().format("YYYYMMDDHHMMSS").toString() + '.xlsx'
    }
}

module.exports = {
    getNewDonations,
    getNewDonationById,
    verify,
    create,
    update,
    deleteById,
    getAll,
    exportData
}