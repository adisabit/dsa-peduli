const models = require('../models/index')
const Post = models['Post']
const constant = require('../constants/post')
const DataNotFoundError = require('../errors/data-not-found')


/**
 * Get latest published education
 * 
 * @param {int} limit 
 */
const getLatestPublishedEducation = async (limit) => {
    return await Post.findAll({
        where: {
            status: constant.STATUS_PUBLISHED,
            category: constant.CATEGORY_EDUCATION
        },
        attributes: ['featuredImageUrl', 'slug', 'title', 'publishedDate'],
        order: [
            ['publishedDate', 'DESC']
        ],
        limit: limit
    })
}

/**
 * Get education by slug
 * 
 * @param {string} slug 
 */
const getBySlug = async (slug) => {
    const education = await Post.findOne({
        where: {
            slug: slug,
            category: constant.CATEGORY_EDUCATION
        },
        attributes: ['featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
    })

    if (!education) {
        throw new DataNotFoundError(204, 'Edukasi tidak ditemukan!')
    }

    return education
}


module.exports = {
    getLatestPublishedEducation,
    getBySlug
}