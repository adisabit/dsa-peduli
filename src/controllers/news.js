const models = require('../models/index')
const Post = models['Post']
const constant = require('../constants/post')
const DataNotFoundError = require('../errors/data-not-found')

/**
 * Get latest published news
 * 
 * @param {int} limit 
 */
const getLatestPublishedNews = async (limit) => {
    return await Post.findAll({
        where: {
            status: constant.STATUS_PUBLISHED,
            category: constant.CATEGORY_NEWS
        },
        attributes: ['featuredImageUrl', 'slug', 'title', 'publishedDate'],
        order: [
            ['publishedDate', 'DESC']
        ],
        limit: limit
    })
}

/**
 * Get news by slug
 * 
 * @param {string} slug 
 */
const getBySlug = async (slug) => {
    const news = await Post.findOne({
        where: { 
            slug: slug,
            category: constant.CATEGORY_NEWS
        },
        attributes: ['featuredImageUrl', 'slug', 'title', 'body', 'publishedDate'],
    })

    if (!news) {
        throw new DataNotFoundError(204, 'Berita tidak ditemukan!')
    }

    return news
}


module.exports = {
    getLatestPublishedNews,
    getBySlug
}