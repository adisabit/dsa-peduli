const models = require('../models/index')
const Subscription = models['Subscription']


const subscribe = async (email) => {
    const subscriber = await Subscription.findOrCreate({
        where: { email: email }
    })

    return subscriber[0]
}


module.exports = {
    subscribe
}