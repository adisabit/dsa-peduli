const models = require('../models/index')
const Donation = models['Donation']
const DonationFidyah = models['DonationFidyah']
const DonationFiveThousand = models['DonationFiveThousand']
const DonationTotal = models['ViewTotalDonation']
const MasterPaymentMethod = models['MasterPaymentMethod']
const dateUtils = require('../utils/date')
const donationConstants = require('../constants/donation')
const MidtransController = require('../controllers/integration/midtrans')
const { generateRandomInt } = require('../utils/number')
const DataNotFoundError = require('../errors/data-not-found')
const moment = require('moment')


const getDonationSummary = async () => {
    // Get latest donation
    const donations = await Donation.findAll({
        attributes: ['donationTime', 'fullName', 'paymentMethod', 'amount', 'isAnonymous'],
        where: { status: donationConstants.STATUS_PAID },
        limit: donationConstants.LATEST_DONATION_NUMBER,
        order: [ ['donationTime', 'DESC'] ]
    });

    // Get collected donation
    const collectedDonation = await DonationTotal.findOne({
        attributes: ['totalDonation']
    })

    // Manipulate donations
    summaries = [];
    donations.forEach((donation, i) => {
        const formattedDateTime = dateUtils.formatDateTime(donation.donationTime)

        let donationItem = {
            donationTime: formattedDateTime,
            fullName: donation.isAnonymous ? "Hamba Allah" : donation.fullName,
            paymentMethod: donation.paymentMethod,
            amount: donation.amount
        }

        summaries.push(donationItem)
    });

    return {
        total: collectedDonation.totalDonation,
        summaries: summaries
    }
}

const donate = async (request) => {
    const req = request.body
    
    // get master payment method
    const masterPaymentMethod = await MasterPaymentMethod.findOne({
        where: {id: req.paymentMethodId}
    })

    if (!masterPaymentMethod) {
        throw new DataNotFoundError(204, 'Metode pembayaran tidak ditemukan!')
    }

    // generate uniqe code
    const uniqueCode = generateRandomInt(1, 1000)

    const generalData = {
        paymentMethodId: req.paymentMethodId,
        paymentMethod: req.paymentMethod,
        category: req.category,
        fullName: req.fullName,
        amount: req.amount,
        uniqueCode: uniqueCode,
        totalAmount: req.amount + uniqueCode,
        isAnonymous: req.isAnonymous,
        status: donationConstants.STATUS_NEW
    }
    

    // Create new donation
    const donation = await Donation.create(generalData)
    donation.dataValues.donationTime = moment(donation.dataValues.donationTime).format()

    let detail;
    let detailResult;

    if (req.category === donationConstants.CATEGORY_FIDYAH) {
        detail = {
            donationId: donation.id,
            address: req.address,
            days: req.days
        }

        detailResult = await DonationFidyah.create(detail)
    } else if (req.category === donationConstants.CATEGORY_5K_MOVEMENT) {
        detail = {
            donationId: donation.id,
            age: req.age,
            domicile: req.domicile
        }

        detailResult = await DonationFiveThousand.create(detail)
    } else {
        detailResult = null
    }

    donation.dataValues.detail = detailResult


    /* -- TEMPORARY DISABLED
    const chargeResult = await MidtransController.charge(
        request,
        donation.id,
        req.category,
        masterPaymentMethod.paymentType,
        masterPaymentMethod.code,
        req.fullName,
        donation.amount
    )

    donation.dataValues.payment = JSON.parse(chargeResult.payments)
    
    if (masterPaymentMethod.paymentType === 'gopay') {
        donation.dataValues.payment = donation.dataValues.payment.slice(0, 2)
    }
    */

    return donation
}


module.exports = {
    getDonationSummary,
    donate
}