const express = require('express')
const app = express()
const { handleError } = require('./src/utils/handler')
const bodyParser = require('body-parser')
const routes = require('./routes/index')
const { successResponse, errorResponse } = require('./src/utils/response')
const cors = require('cors')
const fs = require('fs');
const appRoot = require('app-root-path');
const fileUpload = require('express-fileupload')


// load .env if exist
if (fs.existsSync(`${appRoot}/.env`)) {
    require('dotenv').config()
}

let port
if (process.env.NODE_ENV === 'development') {
    port = process.env.DEV_PORT || 4000
} else if (process.env.NODE_ENV === 'staging') {
    port = process.env.STAGING_PORT || 4100
} else if (process.env.NODE_ENV === 'production') {
    port = process.env.PROD_PORT || 4200
} else {
    // default is dev port
    port = process.env.DEV_PORT || 4000
}

// register routes and body parser
app.use(bodyParser.json({ limit: '5mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }))
app.use(fileUpload({
    limits: { fileSize: 5 * 1024 * 1024 },
    useTempFiles: true,
    tempFileDir: '/tmp'
}))
app.use(cors())
app.use(routes)

// register error handler
app.use((err, req, res, next) => {
    handleError(err, res);
});

// register api not found handler
app.use((req, res, next) => {
    errorResponse(res, "API tidak ditemukan!", 404)
})

// run the application
app.listen(port, () => {
    console.log(`DSA Peduli Berbagi API app listening at http://localhost:${port}`)
}) 