module.exports = {
  apps : [
    {
      name: 'api.dsapeduli.id',
      script: 'app.js',
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ],
  deploy : {
    production : {
      user : process.env.SSH_USER,
      host : process.env.SSH_HOST,
      ref  : 'origin/master',
      repo : process.env.GIT_REPOSITORY,
      path : process.env.DESTINATION_PATH,
      ssh_options: "StrictHostKeyChecking=no",
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && npm run migrate:prod && pm2 reload ecosystem.prod.config.js --env production',
      'pre-setup': ''
    }
  }
};
