module.exports = {
  apps : [
    {
      name: 'api-dev.dsapeduli.id',
      script: 'app.js',
      env: {
        NODE_ENV: 'development'
      }
    }
  ],
  deploy : {
    development : {
      user : process.env.SSH_USER,
      host : process.env.SSH_HOST,
      ref  : 'origin/develop',
      repo : process.env.GIT_REPOSITORY,
      path : process.env.DESTINATION_PATH,
      ssh_options: "StrictHostKeyChecking=no",
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && npm run migrate && pm2 reload ecosystem.config.js --env development',
      'pre-setup': ''
    }
  }
};
