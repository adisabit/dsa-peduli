const fs = require('fs');
const appRoot = require('app-root-path');

if (fs.existsSync(`${appRoot}/.env`)) {
  require('dotenv').config()
}

const env = process.env

module.exports = {
  development: {
    username: env.DB_DEV_USERNAME || 'root',
    password: env.DB_DEV_PASSWORD || '',
    database: env.DB_DEV_NAME || 'express_skeleton',
    host: env.DB_DEV_HOST || '127.0.0.1',
    dialect: env.DB_DEV_DIALECT || 'mariadb',
    timezone: "Asia/Jakarta",
    migrationStorageTableName: "schema_history"
  },
  staging: {
    username: env.DB_STAGING_USERNAME || 'root',
    password: env.DB_STAGING_PASSWORD || '',
    database: env.DB_STAGING_NAME || 'express_skeleton',
    host: env.DB_STAGING_HOST || '127.0.0.1',
    dialect: env.DB_STAGING_DIALECT || 'mariadb',
    timezone: 'Asia/Jakarta',
    migrationStorageTableName: "schema_history"
  },
  production: {
    username: env.DB_PROD_USERNAME,
    password: env.DB_PROD_PASSWORD,
    database: env.DB_PROD_NAME,
    host: env.DB_PROD_HOST,
    dialect: env.DB_PROD_DIALECT || 'mariadb',
    timezone: 'Asia/Jakarta',
    migrationStorageTableName: "schema_history"
  }
};
