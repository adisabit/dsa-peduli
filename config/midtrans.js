const env = process.env

const connectionConfig = {
    development: {
        isProduction: false,
        serverKey: env.MIDTRANS_DEV_SERVER_KEY || 'SB-Mid-server-QRpJxSAO0u-1GtkSgJh0wx68',
        clientKey: env.MIDTRANS_DEV_CLIENT_KEY || 'SB-Mid-client--T1_x5KU-3BA_PQ6'
    },
    production: {
        isProduction: true,
        serverKey: env.MIDTRANS_PROD_SERVER_KEY,
        clientKey: env.MIDTRANS_PROD_CLIENT_KEY
    }
}

module.exports = connectionConfig