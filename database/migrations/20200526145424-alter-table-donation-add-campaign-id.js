'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'donation',
        'campaignId',
        {
          type: Sequelize.STRING,
          after: 'paymentMethodId'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('donation', 'campaignId')
    ]);
  }
};
