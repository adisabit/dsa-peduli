'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'master_payment_method',
        'code',
        {
          type: Sequelize.STRING,
          after: 'imageUrl'
        }
      ),
      queryInterface.addColumn(
        'master_payment_method',
        'paymentType',
        {
          type: Sequelize.STRING,
          after: 'code'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('master_payment_method', 'code'),
      queryInterface.removeColumn('master_payment_method', 'paymentType')
    ]);
  }
};
