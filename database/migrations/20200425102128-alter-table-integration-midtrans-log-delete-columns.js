'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('integration_midtrans_log', 'serverElapsedTime'),
      queryInterface.removeColumn('integration_midtrans_log', 'serverData'),
      queryInterface.removeColumn('integration_midtrans_log', 'serverStatus'),
      queryInterface.removeColumn('integration_midtrans_log', 'responseHeader'),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'integration_midtrans_log',
        'serverElapsedTime',
        {
          type: Sequelize.FLOAT,
          after: 'requestTime'
        }
      ),
      queryInterface.addColumn(
        'integration_midtrans_log',
        'serverData',
        {
          type: Sequelize.TEXT,
          after: 'serverElapsedTime'
        }
      ),
      queryInterface.addColumn(
        'integration_midtrans_log',
        'serverStatus',
        {
          type: Sequelize.STRING,
          after: 'serverData'
        }
      ),
      queryInterface.addColumn(
        'integration_midtrans_log',
        'responseHeader',
        {
          type: Sequelize.STRING,
          after: 'responseCode'
        }
      )
    ]);
  }
};
