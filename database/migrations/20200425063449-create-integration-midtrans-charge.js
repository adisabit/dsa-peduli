'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('integration_midtrans_charge', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transactionId: {
        type: Sequelize.STRING
      },
      orderId: {
        type: Sequelize.STRING
      },
      merchantId: {
        type: Sequelize.STRING
      },
      grossAmount: {
        type: Sequelize.DECIMAL
      },
      currency: {
        type: Sequelize.STRING
      },
      paymentType: {
        type: Sequelize.STRING
      },
      transactionTime: {
        type: Sequelize.DATE
      },
      transactionStatus: {
        type: Sequelize.STRING
      },
      vaNumbers: {
        type: Sequelize.TEXT
      },
      fraudStatus: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('integration_midtrans_charge');
  }
};