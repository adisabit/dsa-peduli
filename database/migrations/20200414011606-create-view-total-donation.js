'use strict';

const viewName = 'view_donation_total'
const query = "SELECT SUM(amount) AS totalDonation FROM donation WHERE status = 'PAID'"

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`CREATE OR REPLACE VIEW ${viewName} AS ${query}`)
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`DROP VIEW ${viewName}`);
  }
};