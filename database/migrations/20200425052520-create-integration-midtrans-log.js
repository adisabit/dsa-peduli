'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('integration_midtrans_log', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.STRING
      },
      requestUrl: {
        type: Sequelize.STRING
      },
      requestMethod: {
        type: Sequelize.STRING
      },
      requestHeader: {
        type: Sequelize.TEXT
      },
      requestBody: {
        type: Sequelize.TEXT
      },
      requestTime: {
        type: Sequelize.DATE
      },
      serverElapsedTime: {
        type: Sequelize.FLOAT
      },
      serverMemoryUsage: {
        type: Sequelize.FLOAT
      },
      serverData: {
        type: Sequelize.TEXT
      },
      serverStatus: {
        type: Sequelize.TEXT
      },
      responseCode: {
        type: Sequelize.STRING
      },
      responseHeader: {
        type: Sequelize.TEXT
      },
      responseBody: {
        type: Sequelize.TEXT
      },
      responseTime: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('integration_midtrans_log');
  }
};