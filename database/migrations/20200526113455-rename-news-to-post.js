'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameTable('news', 'post')
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameTable('post', 'news')
  }
};
