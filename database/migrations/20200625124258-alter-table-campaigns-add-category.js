'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'campaign',
        'category',
        {
          type: Sequelize.STRING,
          after: 'id'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('campaign', 'category')
    ]);
  }
};
