'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('integration_midtrans_charge', 'vaNumbers'),
      queryInterface.addColumn(
        'integration_midtrans_charge',
        'payments',
        {
          type: Sequelize.TEXT,
          after: 'transactionStatus'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('integration_midtrans_charge', 'payments'),
      queryInterface.addColumn(
        'integration_midtrans_charge',
        'vaNumbers',
        {
          type: Sequelize.TEXT,
          after: 'transactionStatus'
        }
      ),
    ]);
  }
};
