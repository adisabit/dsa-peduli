'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'master_payment_method',
        'type',
        {
          type: Sequelize.STRING,
          after: 'id'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('master_payment_method', 'type')
    ]);
  }
};
