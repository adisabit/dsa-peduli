'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('section', 'code', {
      type: Sequelize.STRING,
      after: 'category'
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('section', 'code');
  }
};
