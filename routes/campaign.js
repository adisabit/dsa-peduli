const router = require('express').Router()
const CampaignController = require('../src/controllers/campaign')
const { successResponse } = require('../src/utils/response')

/**
 * Get list of campaigns route
 */
router.get("/", async (req, res, next) => {
    try {
        const result = await CampaignController.getLatestCampaings()

        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Get campaign detail
 */
router.get("/:id", async (req, res, next) => {
    try {
        const result = await CampaignController.getById(req.params.id)

        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Donate to campaign route
 */
router.post("/:id/donate", async (req, res, next) => {
    try {
        const result = await CampaignController.donate(req.params.id, req.body)

        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

module.exports = router