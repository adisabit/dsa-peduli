const router = require('express').Router()
const NewsController = require('../src/controllers/news')
const { successResponse } = require('../src/utils/response')


/**
 * Get latest published news
 */
router.get('/', async (req, res, next) => {
    try {
        const news = await NewsController.getLatestPublishedNews(req.query.limit ? req.query.limit : 5)

        successResponse(res, news, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get news detail by slug
 */
router.get('/:slug', async (req, res, next) => {
    try {
        const news = await NewsController.getBySlug(req.params.slug)

        successResponse(res, news, 200)
    } catch (e) {
        next(e)
    }
})


module.exports = router