const router = require('express').Router()
const SubscriptionController = require('../src/controllers/subscription')
const { successResponse } = require('../src/utils/response')


router.post('/subscribe', async (req, res, next) => {
    try {
        const result = await SubscriptionController.subscribe(req.body.email)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})


module.exports = router