const router = require('express').Router()
const DonationController = require('../src/controllers/donation')
const { successResponse } = require('../src/utils/response')


router.get('/summary', async (req, res, next) => {
    try {
        const summaries = await DonationController.getDonationSummary()
        
        successResponse(res, summaries)
    } catch (e) {
        next(e)
    }
})

router.post('/donate', async (req, res, next) => {
    try {
        const result = await DonationController.donate(req)
        
        successResponse(res, result)
    } catch(e) {
        next(e)
    }
})


module.exports = router