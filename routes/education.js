const router = require('express').Router()
const EducationController = require('../src/controllers/education')
const { successResponse } = require('../src/utils/response')


/**
 * Get latest published news
 */
router.get('/', async (req, res, next) => {
    try {
        const masterPayment = await EducationController.getLatestPublishedEducation(req.query.limit ? req.query.limit : 5)

        successResponse(res, masterPayment, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get news detail by slug
 */
router.get('/:slug', async (req, res, next) => {
    try {
        const masterPayment = await EducationController.getBySlug(req.params.slug)

        successResponse(res, masterPayment, 200)
    } catch (e) {
        next(e)
    }
})


module.exports = router