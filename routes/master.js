const router = require('express').Router()
const MasterController = require('../src/controllers/master')
const { successResponse } = require('../src/utils/response')


router.get('/payment/list', async (req, res, next) => {
    try {
        const masterPayment = await MasterController.getMasterPaymentMethod()

        successResponse(res, masterPayment, 200)
    } catch (e) {
        next(e)
    }
})


module.exports = router