const router = require('express').Router()
const RecapController = require('../../src/controllers/admin/recap')
const { successResponse } = require('../../src/utils/response')


router.get('/', async (req, res, next) => {
    try {
        const recap = await RecapController.getRecap()

        successResponse(res, recap, 200)
    } catch (e) {
        next(e)
    }
})


module.exports = router