const router = require('express').Router()
const AdminDonationController = require('../../src/controllers/admin/donation')
const { successResponse } = require('../../src/utils/response')


/**
 * Get new donations route
 */
router.get('/', async (req, res, next) => {
    try {
        const summaries = await AdminDonationController.getNewDonations() 
        successResponse(res, summaries)
    } catch (error) {
        next(error)
    }
})

/**
 * Get all donation donation route
 */
router.get('/list', async (req, res, next) => {
    try {
        const result = await AdminDonationController.getAll(
            req.originalUrl.split('?')[0],
            req.query.page ? parseInt(req.query.page) : 1,
            req.query.itemPerPage ? parseInt(req.query.itemPerPage) : 8
        )
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Bulk verify donation route
 */
router.put('/verify', async (req, res, next) => {
    try {
        const result = await AdminDonationController.verify(req.body.donations)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Create donation route
 */
router.post('/create', async (req, res, next) => {
    try {
        const result = await AdminDonationController.create(req.body)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Update donation route
 */
router.put('/update', async (req, res, next) => {
    try {
        const result = await AdminDonationController.update(req.body.id, req.body)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Update donation route
 */
router.delete('/delete', async (req, res, next) => {
    try {
        const result = await AdminDonationController.deleteById(req.body.id)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Export as excel
 */
router.get('/export', async (req, res, next) => {
    try {
        const result = await AdminDonationController.exportData(req.query.startDate, req.query.endDate)

        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=" + result.fileName);
        res.end(result.excelFile, 'binary');
    } catch (error) {
        next(error)
    }
})


/**
 * Get new donation by id route
 */
router.get('/:donationId', async (req, res, next) => {
    try {
        const donation = await AdminDonationController.getNewDonationById(req.params.donationId)
        successResponse(res, donation)
    } catch (error) {
        next(error)
    }
})

module.exports = router