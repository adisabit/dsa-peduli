const router = require('express').Router()
const PostController = require('../../src/controllers/admin/post')
const { successResponse } = require('../../src/utils/response')
const constant = require('../../src/constants/post')

/**
 * Create news route
 */
router.post('/create', async (req, res, next) => {
    try {
        const result = await PostController.create(req)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Publish news route
 */
router.put('/publish', async (req, res, next) => {
    try {
        const result = await PostController.publish(req.body.id, constant.CATEGORY_EDUCATION)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Update news route
 */
router.put('/update', async (req, res, next) => {
    try {
        const result = await PostController.update(req.body.id, constant.CATEGORY_EDUCATION, req)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Delete news route
 */
router.delete('/delete', async (req, res, next) => {
    try {
        const result = await PostController.deleteById(req.body.id, constant.CATEGORY_EDUCATION)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Get all published news route
 */
router.get('/published', async (req, res, next) => {
    try {
        const result = await PostController.getPublished(
            req.originalUrl.split('?')[0],
            constant.CATEGORY_EDUCATION,
            req.query.page ? parseInt(req.query.page) : 1,
            req.query.itemPerPage ? parseInt(req.query.itemPerPage) : 8
        )

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Get all draft news route
 */
router.get('/draft', async (req, res, next) => {
    try {
        const result = await PostController.getDraft(
            req.originalUrl.split('?')[0],
            constant.CATEGORY_EDUCATION,
            req.query.page ? parseInt(req.query.page) : 1,
            req.query.itemPerPage ? parseInt(req.query.itemPerPage) : 8
        )

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Get all news route
 */
router.get('/', async (req, res, next) => {
    try {
        const result = await PostController.getAll(constant.CATEGORY_EDUCATION)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Get education by id
 */
router.get('/:id', async (req, res, next) => {
    try {
        const result = await PostController.getById(req.params.id, constant.CATEGORY_EDUCATION)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})



module.exports = router