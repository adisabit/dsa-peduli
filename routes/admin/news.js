const router = require('express').Router()
const PostController = require('../../src/controllers/admin/post')
const { successResponse } = require('../../src/utils/response')
const constant = require('../../src/constants/post')

/**
 * Create news route
 */
router.post('/create', async (req, res, next) => {
    try {
        const result = await PostController.create(req)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Publish news route
 */
router.put('/publish', async (req, res, next) => {
    try {
        const result = await PostController.publish(req.body.id, constant.CATEGORY_NEWS)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Update news route
 */
router.put('/update', async (req, res, next) => {
    try {
        const result = await PostController.update(req.body.id, constant.CATEGORY_NEWS, req)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Delete news route
 */
router.delete('/delete', async (req, res, next) => {
    try {
        const result = await PostController.deleteById(req.body.id, constant.CATEGORY_NEWS)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get all published news route
 */
router.get('/published', async (req, res, next) => {
    try {
        const result = await PostController.getPublished(
            req.originalUrl.split('?')[0],
            constant.CATEGORY_NEWS,
            req.query.page ? parseInt(req.query.page) : 1,
            req.query.itemPerPage ? parseInt(req.query.itemPerPage) : 8
        )

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get all draft news route
 */
router.get('/draft', async (req, res, next) => {
    try {
        const result = await PostController.getDraft(
            req.originalUrl.split('?')[0],
            constant.CATEGORY_NEWS,
            req.query.page ? parseInt(req.query.page) : 1,
            req.query.itemPerPage ? parseInt(req.query.itemPerPage) : 8
        )

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get all news route
 */
router.get('/', async (req, res, next) => {
    try {
        const result = await PostController.getAll(constant.CATEGORY_NEWS)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})

/**
 * Get news detail by id
 */
router.get('/:id', async (req, res, next) => {
    try {
        const result = await PostController.getById(req.params.id, constant.CATEGORY_NEWS)

        successResponse(res, result, 200)
    } catch (e) {
        next(e)
    }
})



module.exports = router