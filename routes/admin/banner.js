const router = require('express').Router()
const AdminBannerController = require('../../src/controllers/admin/banner')
const { successResponse } = require('../../src/utils/response')


/**
 * Create banner route
 */
router.post('/', async (req, res, next) => {
    try {
        const result = await AdminBannerController.create(req)
        successResponse(res, result, 201)
    } catch (error) {
        next(error)
    }
})

/**
 * Get all banners route
 */
router.get('/', async (req, res, next) => {
    try {
        const result = await AdminBannerController.list()
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Get banner detail route
 */
router.get('/:id', async (req, res, next) => {
    try {
        const result = await AdminBannerController.detail(req.params.id)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Update banner route
 */
router.put('/:id', async (req, res, next) => {
    try {
        const result = await AdminBannerController.update(req.params.id, req)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Delete banner route
 */
router.delete('/:id', async (req, res, next) => {
    try {
        const result = await AdminBannerController.deleteById(req.params.id)
        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})


module.exports = router