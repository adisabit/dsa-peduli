const router = require('express').Router()
const AdminSectionController = require('../../src/controllers/admin/section')
const { successResponse } = require('../../src/utils/response')


router.post('/create', async (req, res, next) => {
    try {
        const section = await AdminSectionController.createSection(req.body) 
        successResponse(res, section)
    } catch (e) {
        next(e)
    }
})

router.put('/update', async (req, res, next) => {
    try {
        const result = await AdminSectionController.updateSection(req.body) 
        successResponse(res, result)
    } catch (e) {
        next(e)
    }
})

router.delete('/delete', async (req, res, next) => {
    try {
        const result = await AdminSectionController.deleteSection(req.body.id) 
        successResponse(res, result)
    } catch (e) {
        next(e)
    }
})

router.get('/', async (req, res, next) => {
    try {
        const result = await AdminSectionController.getSection(req.query.category) 
        successResponse(res, result)
    } catch (e) {
        next(e)
    }
})


module.exports = router