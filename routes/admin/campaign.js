const router = require('express').Router()
const AdminCampaignController = require('../../src/controllers/admin/campaign')
const { successResponse } = require('../../src/utils/response')

/**
 * Get new donations route
 */
router.get('/', async (req, res, next) => {
    try {
        const campaigns = await AdminCampaignController.getAll()
         
        successResponse(res, campaigns)
    } catch (error) {
        next(error)
    }
})

/**
 * Get new donations route
 */
router.get('/:id', async (req, res, next) => {
    try {
        const campaign = await AdminCampaignController.getById(req.params.id)

        successResponse(res, campaign)
    } catch (error) {
        next(error)
    }
})

/**
 * Get new donations route
 */
router.post('/create', async (req, res, next) => {
    try {
        const campaign = await AdminCampaignController.create(req)

        successResponse(res, campaign)
    } catch (error) {
        next(error)
    }
})

/**
 * Get new donations route
 */
router.put('/update', async (req, res, next) => {
    try {
        const result = await AdminCampaignController.update(req.body.id, req)

        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})

/**
 * Get new donations route
 */
router.delete('/delete', async (req, res, next) => {
    try {
        const result = await AdminCampaignController.deleteById(req.body.id)

        successResponse(res, result)
    } catch (error) {
        next(error)
    }
})


module.exports = router