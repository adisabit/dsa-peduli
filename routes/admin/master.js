const router = require('express').Router()
const MasterPaymentMethodController = require('../../src/controllers/admin/master')
const { successResponse } = require('../../src/utils/response')

/**
 * Create master payment method
 */
router.post('/payment/', async (req, res, next) => {
    try {
        const result = await MasterPaymentMethodController.create(req.body)

        successResponse(res, result, 201)
    } catch (error) {
        next(error)
    }
})

/**
 * Update master payment method
 */
router.put('/payment/:id', async (req, res, next) => {
    try {
        const result = await MasterPaymentMethodController.update(req.params.id, req.body)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Delete master payment method
 */
router.delete('/payment/:id', async (req, res, next) => {
    try {
        const result = await MasterPaymentMethodController.deleteById(req.params.id)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Delete master payment method
 */
router.get('/payment/', async (req, res, next) => {
    try {
        const result = await MasterPaymentMethodController.list()

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})

/**
 * Delete master payment method
 */
router.get('/payment/:id', async (req, res, next) => {
    try {
        const result = await MasterPaymentMethodController.detail(req.params.id)

        successResponse(res, result, 200)
    } catch (error) {
        next(error)
    }
})


module.exports = router