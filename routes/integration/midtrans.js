const router = require('express').Router()
const MidtransController = require('../../src/controllers/integration/midtrans')
const { successResponse } = require('../../src/utils/response')


router.post('/notification', async (req, res, next) => {
    try {
        const result = await MidtransController.handleNotification(req.body)
        
        successResponse(res, result)
    } catch (e) {
        next(e)
    }
})


module.exports = router