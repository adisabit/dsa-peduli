const router = require('express').Router()
const SectionController = require('../src/controllers/section')
const { successResponse } = require('../src/utils/response')
const sectionConstants = require('../src/constants/section')


router.get('/statistics', async (req, res, next) => {
    try {
        const category = sectionConstants.SECTION_STATISTIC
        const statisticSection = await SectionController.getSectionByCategory(category)

        successResponse(res, statisticSection)
    } catch (error) {
        next(error)
    }
})

router.get('/about-us', async (req, res, next) => {
    try {
        const category = sectionConstants.SECTION_ABOUT_US
        const aboutUsSection = await SectionController.getSectionByCategory(category)

        successResponse(res, aboutUsSection)
    } catch (error) {
        next(error)
    }
})

router.get('/links', async (req, res, next) => {
    try {
        const category = sectionConstants.SECTION_LINK
        const aboutUsSection = await SectionController.getSectionByCategory(category)

        successResponse(res, aboutUsSection)
    } catch (error) {
        next(error)
    }
})


module.exports = router