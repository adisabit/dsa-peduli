const router = require('express').Router()
const storage = require('../src/libs/storage')
const { successResponse } = require('../src/utils/response')
const { v4: uuidv4 } = require('uuid')


const donationRoute = require('./donation')
const masterRoute = require('./master')
const sectionRoute = require('./section')
const subscriptionRoute = require('./subscription')
const newsRoute = require('./news')
const educationRoute = require('./education')
const campaignRoute = require('./campaign')
const bannerRoute = require('./banner')

const integrationMidtransRoute = require('./integration/midtrans')

const adminRecapRoute = require('./admin/recap')
const adminDonationRoute = require('./admin/donation')
const adminSectionRoute = require('./admin/section')
const adminNewsRoute = require('./admin/news')
const adminEducationRoute = require('./admin/education')
const adminCampaignRoute = require('./admin/campaign')
const adminMasterRoute = require('./admin/master')
const adminBannerRoute = require('./admin/banner')

// register routes here
router.use('/donation', donationRoute)
router.use('/master', masterRoute)
router.use('/section', sectionRoute)
router.use('/integration/midtrans', integrationMidtransRoute)
router.use('/subscription', subscriptionRoute)
router.use('/news', newsRoute)
router.use('/education', educationRoute)
router.use('/campaign', campaignRoute)
router.use('/banner', bannerRoute)
router.use('/admin/recap', adminRecapRoute)
router.use('/admin/donation', adminDonationRoute)
router.use('/admin/section', adminSectionRoute)
router.use('/admin/news', adminNewsRoute)
router.use('/admin/education', adminEducationRoute)
router.use('/admin/campaign', adminCampaignRoute)
router.use('/admin/master', adminMasterRoute)
router.use('/admin/banner', adminBannerRoute)

router.post('/image/upload', async (req, res, next) => {
    try {
        const image = req.files.image
        const fileType = image.mimetype.split('/')[1]
        const fileName = uuidv4() + '.' + fileType
        
        const destination = `${req.body.destination.toLowerCase()}/${fileName}`
        
        const imageUrl = await storage.upload(image.tempFilePath, {
            public: true,
            destination: destination,
            metadata: {
                contentType: image.mimetype,
            },
            gzip: true
        })
        
        successResponse(res, {
            imageUrl: imageUrl
        })
    } catch(e) {
        next(e)
    }
})

router.get('/', (req, res) => {
    if (process.env.NODE_ENV === 'development') {
        successResponse(res, "Server development API DSA Peduli Berbagi online!")
    } else if (process.env.NODE_ENV === 'staging') {
        successResponse(res, "Server staging API DSA Peduli Berbagi online!")
    } else if (process.env.NODE_ENV === 'production') {
        successResponse(res, "Server production API DSA Peduli Berbagi online!")
    } else {
        successResponse(res, "Server devepment API DSA Peduli Berbagi online!")
    }
})

module.exports = router