const router = require('express').Router()
const BannerController = require('../src/controllers/banner')
const { successResponse } = require('../src/utils/response')


/**
 * Get active banners route
 */
router.get('/', async (req, res, next) => {
    try {
        const summaries = await BannerController.getActiveBanners()
        successResponse(res, summaries)
    } catch (error) {
        next(error)
    }
})


module.exports = router